import React, {useState} from "react";
import ChannelForm from "../../components/Forms/ChannelForm";
import {addNewChannel} from "../../redux/actions/axiosActions";
import {useHistory} from "react-router-dom";
import IgdbApiConnector from "../../connectors/IgdbApiConnector";
import {connect} from "react-redux";
// import {setLinkActive} from "../../tools/handleLink";

const ChannelCreationPage = ({store, ...props}) => {
    // const [startDate, setStartDate] = useState(new Date());
    const [chanInfo, setChanInfos] = useState({
        name: '',
        platform: '',
        gameName: '',
        server: '',
        gameMode: '',
        nbPlayers: 0
    });
    const [startDate, setStartDate] = useState(new Date())

    const history = useHistory();

    const handleChangeChanForm = e => {
        setChanInfos({
            ...chanInfo,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmitChanForm = e => {
        e.preventDefault();
        // setChanInfos({...chanInfo, startDate: startDate})
        console.log('cest submit')
        console.log(startDate)
        console.log(chanInfo)
        props.dispatch(addNewChannel(chanInfo));
        // alert('Created !');      //TODO: Better creation feedback (page, animation etc..)
        history.push('/');        //TODO: redirect to the new channel (/channels/:id)
        // setLinkActive(document.querySelectorAll('.Navbar-links > li > a')[0]);      //TODO dirty js to change !
    };

    const handleChangeDateChanForm = date => {
        setStartDate(date)
    };

    return (
        <div className="ChannelCreationPage">
            <div className="page-content">

                <h1 className="page-title">Channel Creation Page</h1>

                <ChannelForm
                    startDate={startDate}
                    chanInfo={chanInfo}
                    onChangePicker={handleChangeDateChanForm}
                    onChange={handleChangeChanForm}
                    onSubmit={handleSubmitChanForm}/>
            </div>
        </div>
    );
};

export default connect()(ChannelCreationPage);