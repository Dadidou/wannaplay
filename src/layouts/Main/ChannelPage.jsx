import React from "react";
import {useLocation} from 'react-router-dom';
import ChatBoard from "../../components/Chat/ChatBoard";
import ChannelBar from "../../components/Channels/ChannelBar";
import {connect} from "react-redux";

const ChannelPage = ({...props}) => {
    const location = useLocation();
    // Getting the channel passed in history.state when click on a channel card (ChannelCard.jsx)
    const channel = location.state.chan;

    return (
        <div className="ChannelPage">
            <ChannelBar/>
            <div className="page-content">
                <h1 className="page-title">{channel.name}</h1>
                <h2 className="page-subtitle">Plannify your action !</h2>
                {/*TODO: Utiliser Context pour ne pas balader les props sur 50 niveaux*/}
                <ChatBoard messages={props.messages}/>
            </div>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        messages: state.channels
    }
};

export default connect(
    mapStateToProps
)(ChannelPage);