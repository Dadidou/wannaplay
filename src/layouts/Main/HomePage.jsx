import React, {useEffect} from "react";
import ChannelsList from "../../components/Channels/ChannelsList";
import Loader from "../../components/Loader";
import {connect} from "react-redux";
import {fetchAllChannels} from "../../redux/actions/axiosActions";

const HomePage = ({connectedUser, ...props}) => {

    useEffect(() => {
        if(connectedUser) props.dispatch(fetchAllChannels());
    }, []);

    return (
        <div className="HomePage">
            <div className="page-content">

                <h1 className="page-title">Home Page</h1>

                {/*If not connected, you'll see a message */}
                {connectedUser ?
                     // While waiting the axios promise response, you'll see a cool loader
                    !props.isLoading ?
                        <ChannelsList channels={props.channels}/>
                        :
                        <Loader/>
                    :
                    <span>You must be logged to see channels</span>
                }
            </div>

        </div>
    );
};

const mapStateToProps = state => {
    return {
        isLoading: state.isLoading,
        channels: state.channels
    }
};

export default connect(
    mapStateToProps
)(HomePage);