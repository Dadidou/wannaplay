import React, {useState} from "react";
import RegisterForm from "../../components/Forms/RegisterForm";
import {addNewuser} from "../../redux/actions/axiosActions";
import {connect} from "react-redux";
import {useHistory} from "react-router-dom";
// import {setLinkActive} from "../../tools/handleLink";

const RegisterPage = ({store, ...props}) => {
    const [userInfo, setUserInfos] = useState({
        username: '',
        password: '',
        email: ''
    });

    const history = useHistory();

    const handleChange = e => {
        setUserInfos({
            ...userInfo,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = e => {
        e.preventDefault();
        props.dispatch(addNewuser(userInfo));
        alert('You\'re registered !');      //TODO: Better registration feedback (page, animation etc..)
        history.push('/login');
    }

    return (
        <div className="RegisterPage">
            <div className="page-content">
            <h1 className="page-title">Register Page</h1>

            <RegisterForm
                userInfo={userInfo}
                onSubmit={handleSubmit}
                onChange={handleChange}
            />
            </div>
        </div>
    );
};

export default connect()(RegisterPage);