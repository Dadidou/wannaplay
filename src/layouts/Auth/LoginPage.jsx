import React, {useRef, useState} from "react";
import {useHistory} from 'react-router-dom';
import {connectUser} from "../../redux/actions/axiosActions";
import LoginForm from "../../components/Forms/LoginForm";
import {connect} from "react-redux";
// import {setLinkActive} from "../../tools/handleLink";

const LoginPage = ({...props}) => {
    // TODO: pas de state ni de passage de ref pour les formulaires
    const history = useHistory();

    const loginHandleSubmit = (loginInfo) => {
        console.log(loginInfo)
        props.dispatch(connectUser(loginInfo));
        // alert('You\'re logged !');      //TODO: Better registration feedback (page, animation etc..)
        history.push('/');  //TODO: history redirect only when logged.. Put it in actions ?
        // setLinkActive(document.querySelectorAll('.Navbar-links > li > a')[0]);      //TODO dirty js to change !
    };

    return (
        <div className="LoginPage">
            <div className="page-content">
                <h1 className="page-title">Login Page</h1>

                <LoginForm
                    onSubmit={loginHandleSubmit}
                />
            </div>
        </div>
    );
};

export default connect()(LoginPage);