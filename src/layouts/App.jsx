import React, {useEffect} from 'react';
import {Provider, connect} from 'react-redux';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import HomePage from '../layouts/Main/HomePage';
import UserPage from '../layouts/Main/UserPage';
import ChannelPage from '../layouts/Main/ChannelPage';
import LoginPage from '../layouts/Auth/LoginPage';
import RegisterPage from '../layouts/Auth/RegisterPage';
import Navbar from "../components/Nav/Navbar";
import store from "../redux/store/createStore";
import {disconnectUser} from "../redux/actions/frontActions";
import ChannelCreationPage from "./Main/ChannelCreationPage";

// Params are placeholders in the URL that begin
// with a colon, like the `:id` param defined in
// the route in this example. A similar convention
// is used for matching dynamic segments in other
// popular web frameworks like Rails and Express.

const App = ({...props}) => {

    const handleClickLogout = () => {
        //TODO: route logout ?
        props.dispatch(disconnectUser());
    }

    //TODO: Better routes names
    return (
        // Store prividing
        <Provider store={store}>
            {/*Router providing*/}
            <BrowserRouter>
                <div className="App">
                    <Navbar onClickLogout={handleClickLogout} connectedUser={props.connectedUser}/>

                    <Switch>
                        <Route exact path="/">
                            <HomePage connectedUser={props.connectedUser}/>
                        </Route>
                        <Route path="/login">
                            <LoginPage/>
                        </Route>
                        <Route path="/register">
                            <RegisterPage/>
                        </Route>
                        <Route path="/users/me">
                            <UserPage/> {/*No id in the URL for users*/}
                        </Route>
                        <Route exact path="/channel">
                            <ChannelPage/>
                        </Route>
                        <Route exact path="/channels/new">
                            <ChannelCreationPage/>
                        </Route>
                        {/*<Route component={NotFound}/>*/}

                        {/*<Route path='/channels' component={ChannelsList}/>*/}
                    </Switch>
                </div>
            </BrowserRouter>
        </Provider>
    );
};

const mapStateToProps = state => {
    return {
        connectedUser: state.connectedUser
    }
};

export default connect(
    mapStateToProps
)(App);
