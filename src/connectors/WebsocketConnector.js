import ioClient from 'socket.io-client'

export default ioClient(process.env.ENDPOINT_WEBSOCKET_CONNECTOR);