import axios from "axios";

export default axios.create({
    baseURL: process.env.ENDPOINT_IGDBAPI_CONNECTOR,
    withCredentials: false,                 // TODO:  No CORS ?
    headers: {
        // 'Access-Control-Allow-Origin': process.env.ENDPOINT_IGDBAPI_CONNECTOR,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
});