import React from "react";
import InputBar from "./InputBar";
import MessagesList from "./MessagesList";

const ChatBoard = ({messages, ...props}) => {

    return (
        <div className="ChatBoard">
            this is the chat board
            <MessagesList messages={messages}/>
            <InputBar/>

        </div>
    );
};

export default ChatBoard;