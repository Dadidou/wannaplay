import React from "react";
import ChannelCard from "../Channels/ChannelCard";
import Message from "./Message";

const MessagesList = ({messages, ...props}) => {

    return (
        <div className="MessagesList">
            <ul>
                {messages.map((msg) => {
                    return (
                        <Message
                            message={msg}
                            key={msg._id}
                        />
                    );
                })}

            </ul>
        </div>
    );
};

export default MessagesList;