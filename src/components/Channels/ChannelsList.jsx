import React from 'react';
import ChannelCard from "./ChannelCard";

const ChannelsList = ({channels}) => {

    return (
        <div className="ChannelsList">
            {/* Check to see if any items are found*/}

            {/* Render the list of items */}
            {/*TODO: chan.game.name*/}
            {channels.map((chan) => {
                return (
                    <ChannelCard
                        channel={chan}
                        key={chan._id}
                    />
                );
            })}


        </div>
    );

}

export default ChannelsList;