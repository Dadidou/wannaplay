import React from "react";
import pathImgTest from '../../../public/assets/img/FFcristal.png';
import {wordPluralOrNot} from "../../tools/handefulFunctions";
import {useHistory} from 'react-router-dom';

const ChannelCard = ({channel, ...props}) => {
    let history = useHistory();

    const handleClickCard = () => {
        history.push({
            pathname: '/channel',
            state: {
                chan: channel
            }
        });
    };

    return (
        <div onClick={handleClickCard} className="ChannelCard card">
            <div className="card-sticker">{channel.gameName}</div>
            <img className="card-img-top" src={pathImgTest} alt="FF cristal"/>
            <div className="card-body">
                <h5 className="card-title">{channel.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{channel.nbPlayers + wordPluralOrNot(' player', channel.nbPlayers)}</h6>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">{channel.platform}</li>
                    <li className="list-group-item">{channel.gameMode}</li>
                    <li className="list-group-item">{channel.startDate}</li>
                </ul>

                {/*<a href="#" className="btn btn-primary">Delete ?</a>*/}
            </div>
        </div>
    );
};

export default ChannelCard;





