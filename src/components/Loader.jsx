import React from "react";

const Loader = () => {
    const fill1 = '#d4d4d4';
    const fill2 = '#ffc107';

    return (
        <div className="Loader">
            <svg
                width="200px"
                height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                <g transform="translate(0 -7.5)">
                    <circle cx="50" cy="41" r="12.829" fill={fill1} transform="rotate(216.3 50 50)">
                        <animateTransform attributeName="transform" type="rotate" dur="1s" repeatCount="indefinite"
                                          keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform>
                        <animate attributeName="r" dur="1s" repeatCount="indefinite" calcMode="spline"
                                 keyTimes="0;0.5;1"
                                 values="0;15;0" keySplines="0.2 0 0.8 1;0.2 0 0.8 1"></animate>
                    </circle>
                    <circle cx="50" cy="41" r="2.171" fill={fill2} transform="rotate(396.299 50 50)">
                        <animateTransform attributeName="transform" type="rotate" dur="1s" repeatCount="indefinite"
                                          keyTimes="0;1" values="180 50 50;540 50 50"></animateTransform>
                        <animate attributeName="r" dur="1s" repeatCount="indefinite" calcMode="spline"
                                 keyTimes="0;0.5;1"
                                 values="15;0;15" keySplines="0.2 0 0.8 1;0.2 0 0.8 1"></animate>
                    </circle>
                </g>
            </svg>
        </div>
    );
};


export default Loader;