import React from "react";
import {useLocation, Link} from "react-router-dom";

const Navbar = ({connectedUser, onClickLogout}) => {
    const currentPath = useLocation().pathname

    return (
        //TODO: Better navigation !! With redux !!
        <div className="Navbar">
            <div className="Navbar-brand"> Wannaplay</div>

            <ul className="Navbar-links">
                <li className="Navbar-link">
                    <Link
                        className={currentPath === '/' ? 'link-active' : ''}
                        to="/">home</Link>
                </li>
                <li className="Navbar-link">
                    <Link
                        className={currentPath === '/login' ? 'link-active' : ''}
                        to="/login">login</Link>
                </li>
                <li className="Navbar-link">
                    <Link
                        className={currentPath === '/register' ? 'link-active' : ''}
                        to="/register">register</Link>
                </li>

                {connectedUser &&
                <li className="Navbar-link">
                    <Link
                        className={currentPath === '/channels/new' ? 'link-active' : ''}
                        to="/channels/new">new channel</Link>
                </li>
                }
            </ul>

            {connectedUser &&
            <div className="Navbar-user">
                <span>{connectedUser.username}</span>
                <button className="button-logout btn btn-outline-warning"
                        onClick={onClickLogout} type="button">Logout
                </button>
            </div>
            }
        </div>
    );
};

export default Navbar;
