import React from "react";

const NotFound = () => {
    return (
        <div className="NotFound">

            Sorry, page not found :(

        </div>
    );
};

export default NotFound;