import React, {useRef} from 'react';

const LoginForm = ({onSubmit}) => {
    const usernameRef = useRef();
    const passwordRef = useRef();

    // TODO: Utiliser des variables comme ça plutôt que de tout mettre dans le state.
    //TODO: --> Pas de mise à jour du Cpt nécessaire = pas de state nécessaire.
    let loginInfo = {
        username: '',
        password: ''
    };
    const loginHandleChange = () => {
        loginInfo = {
            username: usernameRef.current.value,
            password: passwordRef.current.value
        };
    };

    return (
        <form className="LoginForm" onSubmit={() => {onSubmit(loginInfo)}}>
            <div className="form-group">
                <label htmlFor="forInputUsename">Username</label>
                <input
                    ref={usernameRef}
                    name="username"
                    placeholder="Enter username"
                    onChange={loginHandleChange}
                    type="text"
                    className="form-control"
                    id="forInputUsename"
                    aria-describedby="usernameDesc"
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="forInputEmail">Password</label>
                <input
                    ref={passwordRef}
                    name="password"
                    placeholder="Enter password"
                    onChange={loginHandleChange}
                    type="password"
                    className="form-control"
                    id="forInputPassword"
                    aria-describedby="passwordDesc"
                    required
                />
                <small id="passwordDesc" className="form-text text-muted">
                    Don't share your password :o
                </small>
            </div>

            <button type="submit" className="button-submit btn btn-warning">Submit</button>
        </form>
    );
}

export default LoginForm;