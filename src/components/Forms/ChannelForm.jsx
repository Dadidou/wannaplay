import React, {useRef, useState} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import PickerCustomInput from "./PickerCustominput";

const ChannelForm = ({chanInfo, startDate, onChangePicker, onChange, onSubmit}) => {

    return (
        <form className="ChannelForm" onSubmit={onSubmit}>

            <div className="form-group">
                <label htmlFor="inputChanName">Channel name</label>
                <input
                    name="name"
                    placeholder="explicit"
                    value={chanInfo.name || ''}
                    onChange={onChange}
                    type="text"
                    className="form-control"
                    id="inputChanName"
                    aria-describedby="nameDesc"
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="inputGame">Game</label>
                <input
                    name="gameName"
                    placeholder="Find your game"
                    value={chanInfo.gameName || ''}
                    onChange={onChange}
                    type="text"
                    className="form-control"
                    id="inputGame"
                    aria-describedby="gameNameDesc"
                    required
                />
            </div>
            <div className="form-group">
                <label className="mr-sm-2" htmlFor="inputPlatform">Platform</label>
                <select
                    name="platform"
                    value={chanInfo.platform || ''}
                    onChange={onChange}
                    className="custom-select mr-sm-2"
                    id="inputPlatform">
                    <option defaultValue="1">Choose...</option>
                    <option value="PC">PC</option>
                    <option value="Steam">Steam</option>
                    <option value="Epic">Epic</option>
                    <option value="Gog">Gog</option>
                    <option value="PS4">PS4</option>
                    <option value="XboxOne">XboxOne</option>
                    <option value="Nintendo Switch">Nintendo Switch</option>
                </select>
            </div>
            <div className="form-group">
                <label className="mr-sm-2" htmlFor="inputServer">Server</label>
                <select name="server" value={chanInfo.server || ''} onChange={onChange}
                        className="custom-select mr-sm-2"
                        id="inputServer">
                    <option defaultValue="1">Choose...</option>
                    {/*TODO: Depends of Game*/}
                    <option value="Server1">Server1</option>
                </select>
            </div>
            <div className="form-group">
                <label className="mr-sm-2" htmlFor="inputMode">Game mode</label>
                <select name="gameMode" value={chanInfo.gameMode || ''} onChange={onChange}
                        className="custom-select mr-sm-2"
                        id="inputMode">
                    <option defaultValue="1">Choose...</option>
                    {/*TODO: Depends of Platform*/}
                    <option value="Mode1">Mode1</option>
                    <option value="Deathmatch">Deathmatch</option>
                </select>
            </div>
            <div className="form-group">
                <label className="mr-sm-2" htmlFor="inputPlayers">Players</label>
                <select name="nbPlayers" value={chanInfo.nbPlayers || ''} onChange={onChange}
                        className="custom-select mr-sm-2"
                        id="inputPlayers">
                    <option defaultValue="1">Choose...</option>
                    {/*TODO: Depends of Game mode*/}
                    <option value="1">1</option>
                    <option value="8">8</option>
                    {/*<option value="3">2</option>*/}
                    {/*<option value="4">3</option>*/}
                </select>
            </div>
            <div className="form-group">
                <label className="mr-sm-2" htmlFor="inputDate">When ?</label>
                <div className="form-control">
                    <DatePicker     //TODO: Custom DateTime picker (only date time selection)
                        //TODO: Auto locale and date format !
                        selected={startDate}
                        onChange={onChangePicker}
                        timeIntervals={5}
                        timeFormat="HH:mm"
                        showPopperArrow={false}
                        dateFormat="MM/dd/yyyy h:mm aa"
                        todayButton="today"
                        timeInputLabel="time"//TODO: Traductions
                        showTimeInput
                        shouldCloseOnSelect={false}
                    />
                </div>
            </div>

            <button type="submit" className="button-submit btn btn-warning">Create channel</button>
        </form>
    );
}

export default ChannelForm;