import React from 'react';

const RegisterForm = ({userInfo, onSubmit, onChange}) => {

    return (
        <form className="LoginForm" onSubmit={onSubmit}>
            <div className="form-group">
                <label htmlFor="forInputUsername">Username</label>
                <input
                    name="username"
                    placeholder="Enter username"
                    value={userInfo.username || ''}
                    onChange={onChange}
                    type="text"
                    className="form-control"
                    id="forInputEmail"
                    aria-describedby="usernameDesc"
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="forInputPassword">Password</label>
                <input
                    name="password"
                    placeholder="Enter password"
                    value={userInfo.password || ''}
                    onChange={onChange}
                    type="password"
                    className="form-control"
                    id="forInputPassword"
                    aria-describedby="passwordDesc"
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="forInputEmail">Email</label>
                <input
                    name="email"
                    placeholder="Enter email"
                    value={userInfo.email || ''}
                    // pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 4}$"
                    onChange={onChange}
                    type="email"
                    className="form-control"
                    id="forInputEmail"
                    aria-describedby="emailDesc"
                    required
                />
            </div>

            <button type="submit" className="button-submit btn btn-warning">Submit</button>
        </form>
    );
}

export default RegisterForm;