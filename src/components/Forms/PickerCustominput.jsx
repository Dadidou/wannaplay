import React from "react";

const PickerCustomInput = ({ value, onClick }) => (
    <button className="picker-custom-input" onClick={onClick}>
        {value}
    </button>
);

export default PickerCustomInput;