import {INITIAL_STATE} from '../constants/initialState';
import {
    DISCONNECT_USER,
    CONNECT_USER,
    ADD_NEW_CHANNEL, FETCH_ALL_CHANNELS, ADD_NEW_USER, START_LOADING, END_LOADING, FETCH_ALL_CHANNELS_SUCCESS, ASYNC_FAILED
} from '../constants/actionTypes';

const mainReducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        //--------------------pure front--------------------------------------------
        case START_LOADING:
            return {        // (immutable state)
                ...state,
                isLoading: true
            } || state;
            break;

        case END_LOADING:
            return {
                ...state,
                isLoading: false
            } || state;
            break;

        //--------------------user--------------------------------------------
        case ADD_NEW_USER_SUCCESS:
            localStorage.setItem('connectedUser', JSON.stringify(action.payload.user));
            return {
                ...state,
                connectedUser: action.payload.user      // When a new user registred itselves, then it will be connected
            } || state;
            break;

        case CONNECT_USER_SUCCESS:
            localStorage.setItem('connectedUser', JSON.stringify(action.payload.user));
            return {
                ...state,
                connectedUser: action.payload.user
            } || state;
            break;

        case DISCONNECT_USER:
            localStorage.removeItem('connectedUser');
            return {
                ...state,
                connectedUser: null
            } || state;
            break;

        //--------------------channels--------------------------------------------
        case ADD_NEW_CHANNEL_SUCCESS:
            return {
                ...state,
                channels: [action.payload.newChan, ...state.channels]  //  [...state.channels, newChan]
            } || state;
            break;

        case FETCH_ALL_CHANNELS_SUCCESS:
            return {
                ...state,
                channels: action.payload.channels.reverse()     // Reverse here !
            } || state;
            break;

        //----------------------------------------------------------------
        default:
            return state;
    }
};

export default mainReducer;


