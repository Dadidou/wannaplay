import {fork} from 'redux-saga/effects'
import websocketSaga from "./websocketsSaga";
import axiosSaga from "./axiosSaga";

function* rootSaga () {
    yield [
        fork(websocketSaga), // saga1 can also yield [ fork(actionOne), fork(actionTwo) ]
        fork(axiosSaga),
    ];
}

export default rootSaga