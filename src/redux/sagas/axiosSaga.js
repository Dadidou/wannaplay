
import {FETCH_ALL_CHANNELS, FETCH_ALL_CHANNELS_SUCCESS, ADD_NEW_CHANNEL, CONNECT_USER} from "../constants/actionTypes";

//TODO: Saga

import AppConnector from "../../connectors/AppConnector";
import {
    addNewUserSuccess,
    startLoading, endLoading
} from "./frontActions";
import {
    addNewUser,
    addNewUserSuccess,
    fetchAllChannels,
    fetchAllChannelsSuccess,
    addNewChannel,
    addNewChannelSuccess,
    connectUser,
    connectUserSuccess
} from "./axiosActions";

import AuthConnector from "../../connectors/AuthConnector";
import store from "../store/createStore";
import { asyncFailed, addNewChannel } from "../actions/axiosActions";
import { call, put } from "redux-saga/effects";

// ============================== App Connector ====================================

function* asyncFetchAllChannels() {
    yield put(startLoading())   //TODO: In reducer or not ? (More explicite and simple here)
    try {
        const channels = yield call(AppConnector.get(`/api/allChannels`))
        yield put(fetchAllChannelsSuccess(channels))
        yield put(endLoading())
    } catch (e) {
        console.log(e.message);
        yield put(endLoading())
    }
}

function* asyncAddNewChannel(action) {
    yield put(startLoading())
    try {
        const newChan = yield call(AppConnector.post(`/api/channels/new`), action.payload.chanInfo)
        yield put(addNewChannelSuccess(newChan))
        yield put(endLoading())
    } catch(e) {
        console.log(e.message);
        yield put(endLoading())
    }
}

// ============================== Auth Connector ====================================

function* asyncConnectUser(action) {
    yield put(startLoading())
    try {
        const user = yield call(AuthConnector.post(`/api/login`), JSON.stringify(action.payload.loginInfo))
        yield put(connectUserSuccess(user))
        yield put(endLoading())
    } catch(e) {
        console.log(e.message)
        yield put(endLoading())
    }
}

function* asyncAddNewUser(action) {
    yield put(startLoading())
    try {
        const user = yield call(AuthConnector.post(`/api/register`), JSON.stringify(action.payload.userInfo))
        yield put(addNewUserSuccess(user))
        yield put(endLoading())
    } catch(e) {
        console.log(e.message)
        yield put(endLoading())
    }
}

// The saga to providing
function* axiosSaga() {
    // takeLatest to keep the latest async result if there are multiple calls
    yield takeLatest(fetchAllChannels.type, asyncFetchAllChannels);     // First arg = string
    yield takeLatest(addNewChannel.type, asyncAddNewChannel);
    yield takeLatest(connectUser.type, asyncConnectUser);
    yield takeLatest(addNewUser.type, asyncAddNewUser);
}

export default axiosSaga;