import {createStore, applyMiddleware} from 'redux';
import reducer from '../reducers/mainReducer';
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas/rootSaga';
import {INITIAL_STATE} from '../constants/initialState'

// Create the saga middleware
const sagaMiddleware = createSagaMiddleware()

// Create store with reducers, global state and middlewares
const store = createStore(reducer, INITIAL_STATE, applyMiddleware(sagaMiddleware))

// Run the saga
sagaMiddleware.run(rootSaga)

export default store;