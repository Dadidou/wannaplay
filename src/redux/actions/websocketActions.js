// TODO: Saga

import socket from "../../connectors/WebsocketConnector";
import store from "../store/createStore";

socket.on('chat message', function (data) {
    $('#messages').append($('<li>').html(`<strong> ${data.nickname} </strong> : ${data.message}`)); // Construct a message with nickname and message from the object data passed in emit from srv
    $('#messages').scrollTop($('#messages').prop('scrollHeight'));  //Scroll to bottom
});