import {DISCONNECT_USER, CONNECT_USER, ADD_NEW_CHANNEL, FETCH_ALL_CHANNELS, FETCH_ALL_CHANNELS_SUCCESS, ADD_NEW_USER, START_LOADING, END_LOADING} from "../constants/actionTypes";

import AuthConnector from "../../connectors/AuthConnector";
import store from "../store/createStore";
import AppConnector from "../../connectors/AppConnector";

export const startLoading = () => {
    return {
        type: START_LOADING
    }
};

export const endLoading = () => {
    return {
        type: END_LOADING
    }
};

export const disconnectUser = () => {
    return {
        type: DISCONNECT_USER
    }
};




