import {FETCH_ALL_CHANNELS, FETCH_ALL_CHANNELS_SUCCESS, ASYNC_FAILED, CONNECT_USER_SUCCESS, CONNECT_USER} from "../constants/actionTypes";

export const fetchAllChannels = () => ({
    type: FETCH_ALL_CHANNELS
});
export const fetchAllChannelsSuccess = channels => ({
    type: FETCH_ALL_CHANNELS_SUCCESS,
    payload: {
        channels: channels
    }
});

export const addNewChannel = (chanInfo) => ({
    type: ADD_NEW_CHANNELS,
    payload: {
        chanInfo: chanInfo
    }
});
export const addNewChannelSuccess = newChan => ({
    type: ADD_NEW_CHANNEL_SUCCESS,
    payload: {
        newChan: newChan
    }
});

export const connectUser = loginInfo => ({
    type: CONNECT_USER,
    payload: {
        loginInfo: loginInfo
    }
});
export const connectUserSuccess = user => ({
    type: CONNECT_USER_SUCCESS,
    payload: {
        user: user
    }
});

export const addNewUser = userInfo => ({
    type: ADD_NEW_USER,
    payload: {
        userInfo: userInfo
    }
});
export const addNewUserSuccess = user => ({
    type: ADD_NEW_USER_SUCCESS,
    payload: {
        user: user
    }
});